import React from 'react';
import { NavLink } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import config from '../../config/ConfigApi.json';
const CommentWithLikes = ({ intl, className, data }) => {
  const getLikeLabel = (likeCount) => {
    if (likeCount === 1) {
      return intl.messages['pages.like'];
    }
    return intl.messages['pages.likes'];
  };

  return (
    <div
      className={`d-flex flex-row mb-3 border-bottom justify-content-between ${className}`}
    >
      <NavLink to="#" location={{}}>
        { <img
          src={config.baseurl+"user/"+data.foto}
          alt={'name'}
          className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
        /> }
      </NavLink>
      <div className="pl-3 flex-grow-1">
        <NavLink to="#" location={{}}>
          <p className="font-weight-medium mb-0">{data.nombre}</p>
          <p className="text-muted mb-0 text-small">{data.stamp}</p>
        </NavLink>
        <p className="mt-3">{data.titulo}</p>
      </div>
      <div className="comment-likes">
        <span className="post-icon">
          {/* <NavLink to="#" location={{}}>454
            
            <i className="simple-icon-heart ml-2" />
          </NavLink> */}
        </span>
      </div>
    </div>
  );
};

export default injectIntl(CommentWithLikes);
