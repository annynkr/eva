/* eslint-disable react/no-array-index-key */
import React,{useState} from 'react';
import { NavLink } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import {
  Card,
  CardBody,
  InputGroup,
  InputGroupAddon,
  Input,
  Button,
} from 'reactstrap';
import SingleLightbox from '../pages/SingleLightbox';
import VideoPlayer from '../common/VideoPlayer';
import CommentWithLikes from '../pages/CommentWithLikes';
import config from '../../config/ConfigApi.json';
import {useDispatch,useSelector} from 'react-redux';
import {ComentarioPost,InsertMegusta} from '../../Dusk/DashboardDusk';


const renderContent = (data) => {
  if (data.type !== 'image') {
    return (
      <SingleLightbox
      thumb={data.image}
      large={data.image}
      className="img-fluid border-0 border-radius mb-3"
      />
      );
    }
    if (data.type === 'video') {
      return (
        <VideoPlayer
        autoplay={false}
        controls
        className="video-js card-img video-content mb-3"
        poster={data.image}
        sources={[
          {
            src: data.video,
            type: 'video/mp4',
          },
        ]}
        />
        );
      }
      return <></>;
    };
    
    const renderComments = (data) => {
      return data.map((item, index) => {
        return <CommentWithLikes data={item} key={index} />;
      });
    };
    
    const Post = ({ data, className, intl }) => {
     
      const disparador=useDispatch();
      const Dash1=useSelector(store=>store.Dash);
      const { messages } = intl;

      const funki=(id)=> {
        disparador(InsertMegusta(Dash1.user.user[0].id_user,id));
      }
      
      const renderLikeAndCommentCount = (data1,data2,data3) => {
        return (
          <div className="mb-3">
            <div className="post-icon mr-3 d-inline-block">
              <NavLink to="#" onClick={()=>funki(data3)} >
                <i className="simple-icon-heart mr-1" />
              </NavLink>
              <span>{data1} </span>
            </div>
      
            <div className="post-icon mr-3 d-inline-block">
              <NavLink to="#" location={{}}>
                <i className="simple-icon-bubble mr-1" />
              </NavLink>
              <span>{data2} </span>
            </div>
          </div>
        );
      };

      const Focus1=(titulo,post)=>{
    disparador(ComentarioPost(titulo,Dash1.user.user[0].id_user,post));
    settitulo("");
    // console.log(titulo,Dash1.user.user[0].id_user,post,"Sssssssssssssssssssssssssssssss");
  }
  const [titulo,settitulo] = useState();
  return (
    <Card className={className}>
      <CardBody>
        <div className="d-flex flex-row mb-3">
          <NavLink to="#" location={{}}>
            <img
              src={config.baseurl+"user/"+data.post.foto}
              alt="thumbnail"
              className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
            />
          </NavLink>
          <div className="pl-3">
            <NavLink to="#" location={{}}>
              <p className="font-weight-medium mb-0 ">{data.post.nombre}</p>
              <p className="text-muted mb-0 text-small">{data.post.stamp}</p>
            </NavLink>
          </div>
        </div> 
        <p>{data.post.mensaje}</p>
        {/* { renderContent(data)} */}
        {data.post.fotopost!="none.jpg"?<SingleLightbox
          thumb={config.baseurl+"uploads/"+data.post.fotopost}
          large={config.baseurl+"uploads/"+data.post.fotopost}
          className="img-fluid border-0 border-radius mb-3"
          />:null}
        
        {renderLikeAndCommentCount(data.postlike[0].gusta,data.comentario.length,data.post.id_post)} 
        <div className="mt-5 remove-last-border">{renderComments(data.comentario)}</div>
        <InputGroup className="comment-container">
        <Input onChange={(e)=>{settitulo(e.target.value)}} value={titulo} /> 
          <InputGroupAddon addonType="append">
            <Button color="primary" onClick={()=>Focus1(titulo,data.post.id_post)}>
              <span className="d-inline-block">Enviar</span>{' '}
              <i className="simple-icon-arrow-right ml-2" />
            </Button>
          </InputGroupAddon>
        </InputGroup>
      </CardBody>
    </Card>
  );
};

export default injectIntl(React.memo(Post));
