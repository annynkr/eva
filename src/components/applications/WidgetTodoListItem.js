import React from 'react';
import { CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';

import { Colxx } from '../common/CustomBootstrap';

const WidgetTodoListItem = ({ item, handleCheckChange, isSelected }) => {
  return (
    <Colxx xxs="12">
        <div className="d-flex flex-grow-1 min-width-zero mb-3 pb-3 border-bottom">
            <NavLink
              to="#"
              location={{}}
              id={`toggler${item.id_user}`}
              className="list-item-heading mb-0 truncate w-80 w-xs-100  mb-1 mt-1"
            >
               <span className="colorTheme"
                style={{
                  width: "10px", 
                  height: "10px",
                  borderRadius: "50%",
                  marginTop: "5px",
                  display: "inline list-item",
                }}></span>
              <span className="align-middle d-inline-block"> {item.titulo}</span>
              <p className="mb-1 ml-3 text-muted text-small">
                {item.stamp}
              </p>
            </NavLink>
         
          <div className="custom-control custom-checkbox align-self-center mr-4">
            <CustomInput
              className="itemCheck mb-0"
              type="checkbox"
              id={`check_${item.id}`}
              checked={isSelected}
              onChange={(event) => handleCheckChange(event, item.id)}
            />
          </div>
        </div>
    </Colxx>
  );
};

export default React.memo(WidgetTodoListItem);
