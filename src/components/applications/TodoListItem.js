import React from 'react';
import { Card, CardBody, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';

import { Colxx } from '../common/CustomBootstrap';

const TodoListItem = ({ item, handleCheckChange, isSelected }) => {
  return (
    <Colxx xxs="12">
      <Card className="card d-flex mb-3">
        <div className="d-flex flex-grow-1 min-width-zero">
          <CardBody className="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
            <NavLink
              to="#"
              location={{}}
              id={`toggler${item.id_pendiente}`}
              className="list-item-heading mb-0 truncate w-80 w-xs-100  mb-1 mt-1"
            >
              {/* <i
                className={`${
                  item.status === 'COMPLETED'
                    ? 'simple-icon-check heading-icon'
                    : 'simple-icon-refresh heading-icon'
                }`}
              /> */}
               <span className="colorTheme"
                style={{
                  width: "10px", 
                  height: "10px",
                  borderRadius: "50%",
                  marginTop: "5px",
                  display: "inline list-item",
                }}></span>
              <span className="align-middle d-inline-block"> Titulo: {item.titulo}</span>
              <p className="mr-4 mb-1 text-muted text-small w-xs-100">
                Fecha de Modificación: {item.stamp}
              </p>
              <p className="mr-4 mb-1 text-muted text-small w-xs-100">
                Fecha de Creacion: {item.fech_creacion}
              </p>
            </NavLink>
          </CardBody>
          <div className="custom-control custom-checkbox pl-1 align-self-center mr-4">
            <CustomInput
              className="itemCheck mb-0"
              type="checkbox"
              id={`check_${item.id_pendiente}`}
              checked={item.estado != "0"? true : false}
              onChange={(event) => handleCheckChange(event, item.id_pendiente)}
              label=""
            />
          </div>
        </div>
      </Card>
    </Colxx>
  );
};

export default React.memo(TodoListItem);
