import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const Informacion = React.lazy(() =>
  import(/* webpackChunkName: "informacion" */ './informacion')
);

const Info = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/informacion`} />
      <Route
        path={`${match.url}/informacion`}
        render={(props) => <Informacion {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default Info;
