/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Row, Card, CardBody, CardTitle } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import {
  Separator,
  Colxx,
} from '../../../components/common/CustomBootstrap';
import SingleLightbox from '../../../components/pages/SingleLightbox';
import VideoPlayer from '../../../components/common/VideoPlayer';
import { dataInteres } from '../../../data/dataInteres';
import IntlMessages from '../../../helpers/IntlMessages';
import SoporteTecnico from '../../../containers/dashboards/SoporteTecnico';

const Informacion = ({ match }) => {
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.info" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" md="12" xl="7" className="col-left">
          <Card className="mb-4">
            <SingleLightbox
              thumb="/assets/logos/white.svg"
              large="/assets/logos/white.svg"
              className="responsive border-0 card-img-top mb-5 mt-5"
            />
            <CardBody>
              <div className="mb-5">
                <h5 className="card-title">Game Changing Features</h5>
                <p>
                  Blended value human-centered social innovation resist scale
                  and impact issueoutcomesbandwidth efficient. A; social return
                  on investment, change-makers, support a,co-createcommitment
                  because sustainable. Rubric when vibrant black lives matter
                  benefitcorporation human-centered. Save the world,
                  problem-solvers support silo massincarceration. Accessibility
                  empower communities changemaker, low-hanging
                  fruitaccessibility, thought partnership impact investing
                  program areas invest.Contextualizeoptimism unprecedented
                  challenge, empower inclusive. Living a fully ethical life
                  theresistance segmentation social intrapreneurship efficient
                  inspire external partners.Systems thinking correlation, social
                  impact; when revolutionary bandwidth. Engaging,revolutionary
                  engaging; empower communities policymaker shared unit of
                  analysistechnology inspiring social entrepreneurship.
                </p>
                <p>
                  Mass incarceration, preliminary thinking systems thinking
                  vibrant thought leadershipcorporate social responsibility.
                  Green space global, policymaker; shared
                  valuedisruptsegmentation social capital. Thought partnership,
                  optimism citizen-centeredcommitment,relief scale and impact
                  the empower communities circular. Contextualize boots on
                  theground; uplift big data, co-creation co-create segmentation
                  youth inspire. Innovateinnovate overcome injustice.
                </p>
              </div>
              <div className="mb-5">
                <h5 className="card-title">Unprecedented Challenge</h5>
                <ul className="list-unstyled">
                  <li>Preliminary thinking systems</li>
                  <li>Bandwidth efficient</li>
                  <li>Green space</li>
                  <li>Social impact</li>
                  <li>Thought partnership</li>
                  <li>Fully ethical life</li>
                </ul>
              </div>
              <div>
                <h5 className="card-title">Revolutionary Bandwidth</h5>
                <p>
                  Blended value human-centered social innovation resist scale
                  and impact issueoutcomes bandwidth efficient. A; social return
                  on investment, change-makers, supporta, co-create commitment
                  because sustainable. Rubric when vibrant black lives
                  matterbenefit corporation human-centered. Save the world,
                  problem-solvers support silomass incarceration. Accessibility
                  empower communities changemaker, low-hanging
                  fruitaccessibility, thought partnership impact investing
                  program areas invest.Contextualize optimism unprecedented
                  challenge, empower inclusive. Living a fullyethical life the
                  resistance segmentation social intrapreneurship efficient
                  inspireexternal partners. Systems thinking correlation, social
                  impact; when revolutionarybandwidth. Engaging, revolutionary
                  engaging; empower communities policymaker sharedunit of
                  analysis technology inspiring social entrepreneurship.Mass
                  incarceration,preliminary thinking systems thinking vibrant
                  thought leadership corporate socialresponsibility. Green space
                  global, policymaker; shared value disrupt segmentationsocial
                  capital. Thought partnership, optimism citizen-centered
                  commitment, reliefscale and impact the empower communities
                  circular. Contextualize boots on theground; uplift big data,
                  co-creation co-create segmentation youth inspire.
                  Innovateinnovate overcome injustice.
                </p>
                <p>
                  Systems thinking correlation, social impact; when
                  revolutionary bandwidth. Engaging,revolutionary engaging;
                  empower communities policymaker shared unit of
                  analysistechnology inspiring social entrepreneurship. Thought
                  partnership, optimismcitizen-centeredcommitment,relief scale
                  and impact the empower communities circular. Contextualize
                  boots on theground; uplift big data, co-creation co-create
                  segmentation youth inspire. Innovateinnovate overcome
                  injustice.
                </p>
              </div>
            </CardBody>
          </Card>
        </Colxx>

        <Colxx xxs="12" md="12" xl="5" className="col-left">
          <Card className="mb-4">
            <CardBody className="p-0">
              <VideoPlayer
                autoplay={false}
                controls
                controlBar={{
                  pictureInPictureToggle: false,
                }}
                className="video-js side-bar-video card-img-top"
                poster="/assets/img/video/poster.jpg"
                sources={[
                  {
                    src:
                      'http://img-ys011.didistatic.com/static/didiglobal/do1_pcUZZjSG7vFlMbdr8fA6#.mp4',
                    type: 'video/mp4',
                  },
                ]}
              />
            </CardBody>
            <CardBody>
              <p className="list-item-heading mb-4">
                Homemade Cheesecake with Fresh Berries and Mint
              </p>
              <footer>
                <p className="text-muted text-small mb-0 font-weight-light">
                  09.04.2018
                </p>
              </footer>
            </CardBody>
          </Card>
          
          <SoporteTecnico />
          
          <Card className="mb-4 mt-4">
            <CardBody>
              <CardTitle>
                <IntlMessages id="info.adicional" />
              </CardTitle>
              {dataInteres.map((item, index) => {
                return (
                  <div
                    className="d-flex flex-row align-items-center mb-3"
                    key={`item${index}`}
                  >
                    <NavLink to={item.link}>
                      <i
                        className={`initial-height simple-icon-info`}
                      />
                    </NavLink>
                    <div className="pl-3 pt-2 pr-2 pb-2">
                      <NavLink to={item.link}>
                        <p className="list-item-heading mb-1">
                          {item.title}
                        </p>
                      </NavLink>
                    </div>
                  </div>
                );
              })}
            </CardBody>
          </Card>
        </Colxx>
      </Row>
    </>
  );
};

export default Informacion;
