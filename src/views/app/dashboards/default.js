import React,{useEffect,useState} from 'react';
import { injectIntl } from 'react-intl';
import { Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import GradientCardContainer from '../../../containers/dashboards/GradientCardContainer';
import SortableStaticticsRow from '../../../containers/dashboards/SortableStaticticsRow';
import AdvancedSearch from '../../../containers/dashboards/AdvancedSearch';
import ClientesPolarArea from '../../../containers/clientes/ClientesPolarArea';
import WidgetNota from '../../../containers/dashboards/WidgetNota';
import Pendientes from '../../../containers/dashboards/Pendientes';
import BienvenidaUser from '../../../containers/dashboards/BienvenidaUser';
import Social from '../../../containers/dashboards/Social';
import ContactToUser from '../../../containers/dashboards/ContactToUser';
import Config from '../../../config/ConfigApi.json';
import {useDispatch,useSelector} from 'react-redux';
import {DashboardCentral,ChatObjetivo} from '../../../Dusk/DashboardDusk';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// import { info } from 'node-sass';
const DefaultDashboard = ({ intl, match }) => {
  const { messages } = intl;
  // const [Dash,SetDash]=useState();
  
  const disparador=useDispatch();
  const Dash1=useSelector(store=>store.Dash);
  
  useEffect(() => {
    console.log(Dash1);
    disparador(DashboardCentral(Dash1.user.user?Dash1.user.user[0].id_user:null));
    disparador(ChatObjetivo(Dash1.user.user?Dash1.user.user[0].id_user:null))
  },[])
  
  return (
    <>
    <ToastContainer />
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.dashboards" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx sm="12" md="12" lg="4" className="mb-4">
        {!Dash1.Central.log?"Cargando...":<BienvenidaUser info={Dash1} />}
        </Colxx>
        <Colxx sm="12" md="6" lg="4" className="mb-4">
          <GradientCardContainer />
        </Colxx>
        <Colxx sm="12" md="6" lg="4" className="mb-4">
          {!Dash1.Central.nota? "Cargando..." : <WidgetNota info={Dash1.Central.nota}/>}
        </Colxx>
      </Row>

      <SortableStaticticsRow messages={messages} />

      <Row>
        <Colxx lg="12" md="12" xl="4">
          <Row>
            <Colxx lg="6" md="6" xl="12" className="mb-4">
              {!Dash1.Central.Pendiente? "Cargando...": <Pendientes info={Dash1.Central.Pendiente} />}
            </Colxx>
            <Colxx lg="6" md="6" xl="12" className="mb-4">
              <ClientesPolarArea chartClass="dashboard-donut-chart" />
            </Colxx>
          </Row>
        </Colxx>

        <Colxx lg="12" md="12" xl="4">
          <Row>
            <Colxx lg="6" xl="12" className="mb-4">
              <AdvancedSearch messages={messages} />
            </Colxx>
            <Colxx lg="6" xl="12" className="mb-4">
              {!Dash1.Central.user? "Cargando..." : <ContactToUser info={Dash1.Central.user} />}
            </Colxx>
          </Row>
        </Colxx>
        
        <Colxx lg="12" md="12" xl="4" className="mb-4">
        {!Dash1.Central.post? "Cargando..." : <Social info={Dash1.Central.post}/>}
        </Colxx>
      </Row>
    </>
  );
};
export default injectIntl(DefaultDashboard);
