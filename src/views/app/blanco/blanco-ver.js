/* eslint-disable react/no-array-index-key */
import React, {useState} from 'react';
import { Row, Card, CardBody, Button, FormGroup,
  Label,
  Input,
  Form, } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import AddNewModal from '../../../containers/modals/blanco/AddNewModal';
import IntlMessages from '../../../helpers/IntlMessages';
import { adminRoot } from '../../../constants/defaultValues';
import {
  Separator,
  Colxx,
} from '../../../components/common/CustomBootstrap';
import SingleLightbox from '../../../components/pages/SingleLightbox';
import Nota from '../../../containers/dashboards/Nota';
import NewNota from '../../../containers/dashboards/NewNota';
import ConfimModal from '../../../containers/modals/ConfirmModal';

const BlancoVer = ({ match }) => {
    // const { messages } = intl;
    const categories = [
        { label: 'Cakes', value: 'Cakes', key: 0 },
        { label: 'Cupcakes', value: 'Cupcakes', key: 1 },
        { label: 'Desserts', value: 'Desserts', key: 2 },
    ];

    const [modalOpen, setModalOpen] = useState(false);
    const [modalConfirmOpen, setModalConfirmOpen] = useState(false);
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.blanco-ver" match={match} />
          <div className="top-right-button-container">
            <Button 
                outline
                color="danger"
                size="sm"
                className="top-right-button mb-2"
                onClick={() => setModalConfirmOpen(!modalConfirmOpen)}
            >
              <IntlMessages id="blanco.delete" />
            </Button>{' '}

            <Button 
              color="primary"
              className="top-right-button mb-2 mr-2"
              size="sm"
              onClick={() => setModalOpen(!modalOpen)}
            >
              <IntlMessages id="blanco.edit" />
            </Button>{' '}
          </div>
          <Separator className="mb-5" />

        </Colxx>
      </Row>

      <Row>
        <Colxx xxs="12" md="12" xl="6" className="col-left">
          <Card className="mb-4">
            <CardBody className="p-0">
                <SingleLightbox
                thumb="/assets/img/details/5.jpg"
                large="/assets/img/details/5.jpg"
                className="responsive border-0 card-img-top mb-3"
                />
            </CardBody>
            <CardBody>
              <footer>
                <p className="text-muted text-small mb-0 font-weight-light">
                  09.04.2018
                </p>
              </footer>
            </CardBody>
            <Separator className="mb-5" />

            <CardBody>
              <div className="mb-5">
                <h5 className="card-title">Datos</h5>
                <Form disabled>
                <FormGroup row>
                  <Colxx sm={6}>
                    <FormGroup>
                      <Label for="exampleEmailGrid">
                        <IntlMessages id="forms.email" />
                      </Label>
                      <Input
                        type="email"
                        name="exampleEmailGrid"
                        id="exampleEmailGrid"
                        disabled
                        // placeholder={messages['forms.email']}
                      />
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={6}>
                    <FormGroup>
                      <Label for="examplePasswordGrid">
                        <IntlMessages id="forms.password" />
                      </Label>
                      <Input
                        type="password"
                        name="examplePasswordGrid"
                        id="examplePasswordGrid"
                        disabled
                        // placeholder={messages['forms.password']}
                      />
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={12}>
                    <FormGroup>
                      <Label for="exampleAddressGrid">
                        <IntlMessages id="forms.address" />
                      </Label>
                      <Input
                        type="text"
                        name="exampleAddressGrid"
                        id="exampleAddressGrid"
                        disabled
                        // placeholder={messages['forms.address']}
                      />
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={12}>
                    <FormGroup>
                      <Label for="exampleAddress2Grid">
                        <IntlMessages id="forms.address2" />
                      </Label>
                      <Input
                        type="text"
                        name="exampleAddress2Grid"
                        id="exampleAddress2Grid"
                        disabled
                        // placeholder={messages['forms.address']}
                      />
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={6}>
                    <FormGroup>
                      <Label for="exampleEmailGrid">
                        <IntlMessages id="forms.city" />
                      </Label>
                      <Input
                        type="text"
                        name="exampleTextGrid"
                        id="exampleTextGrid"
                        disabled
                        // placeholder={messages['forms.city']}
                      />
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={4}>
                    <FormGroup>
                      <Label>
                        <IntlMessages id="forms.state" />
                      </Label>
                      <Input type="select" disabled>
                        <option>Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                        <option>Option 4</option>
                        <option>Option 5</option>
                      </Input>
                    </FormGroup>
                  </Colxx>

                  <Colxx sm={2}>
                    <FormGroup>
                      <Label for="exampleZipGrid">
                        <IntlMessages id="forms.zip" />
                      </Label>
                      <Input
                        type="text"
                        name="exampleZipGrid"
                        id="exampleZipGrid"
                        disabled
                        // placeholder={messages['forms.zip']}
                      />
                    </FormGroup>
                  </Colxx>
                </FormGroup>
              </Form>
              </div>
            </CardBody>
          </Card>
        </Colxx>
      
        <Colxx xxs="12" md="12" xl="6" className="col-left">
          <Row>
            <Colxx md="12" className="mb-4">
              <Nota />
            </Colxx>
          </Row>

          <Row>
            <Colxx md="12"className="mb-4">
              <NewNota />
            </Colxx>
          </Row>
        </Colxx>
      </Row>

        <NavLink to={`${adminRoot}/blanco/blanco-list`} className="w-sm-100">
            <Button outline color="primary" className="mb-2">
                <IntlMessages id="blanco.back" />
            </Button>{' '}
        </NavLink> 

        <AddNewModal
          modalOpen={modalOpen}
          toggleModal={() => setModalOpen(!modalOpen)}
          categories={categories}
        />

        <ConfimModal
          modalOpen={modalConfirmOpen}
          toggleModal={() => setModalConfirmOpen(!modalConfirmOpen)}
        />
    </>
  );
};

export default BlancoVer;