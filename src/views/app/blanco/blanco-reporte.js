import React from 'react';
import {
  Row,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
} from 'reactstrap';
import { injectIntl } from 'react-intl';

import Breadcrumb from '../../../containers/navs/Breadcrumb';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import RadialProgressCard from '../../../components/cards/RadialProgressCard';
import SmallLineCharts from '../../../containers/dashboards/SmallLineCharts';
import WebsiteVisitsChartCard from '../../../containers/dashboards/WebsiteVisitsChartCard';

const ReportePages = ({ match, intl }) => {

  const { messages } = intl;

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <h1><IntlMessages id="menu.blanco-reporte" /></h1>
          <div className="text-zero top-right-button-container">
            <UncontrolledDropdown>
              <DropdownToggle
                caret
                color="primary"
                size="lg"
                outline
                className="top-right-button top-right-button-single mb-2"
              >
                <IntlMessages id="pages.actions" />
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem header>
                  <IntlMessages id="pages.header" />
                </DropdownItem>
                <DropdownItem disabled>
                  <IntlMessages id="pages.delete" />
                </DropdownItem>
                <DropdownItem>
                  <IntlMessages id="pages.another-action" />
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  <IntlMessages id="pages.another-action" />
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </div>

          <Breadcrumb match={match} />
          <Separator className="mb-5" />

          <Row>
                <Colxx xxs="12" lg="12" className="mb-4">
                  <Row>
                    <Colxx xxs="12" md="4" className="mb-4">
                      <RadialProgressCard
                        className="mb-4"
                        title={messages['pages.order-status']}
                        percent={85}
                        isSortable={false}
                      />
                    </Colxx>
                    <Colxx xxs="12" md="4" className="mb-4">
                      <RadialProgressCard
                        className="mb-4"
                        title={messages['pages.order-status']}
                        percent={40}
                        isSortable={false}
                      />
                    </Colxx>
                    <Colxx xxs="12" md="4" className="mb-4">
                      <RadialProgressCard
                        className="mb-4"
                        title={messages['pages.order-status']}
                        percent={85}
                        isSortable={false}
                      />
                    </Colxx>
                  </Row>
                  <SmallLineCharts itemClass="dashboard-small-chart-analytics" />
                </Colxx>

                <Colxx xxs="12" lg="12">
                  <WebsiteVisitsChartCard className="mb-4" controls={false} />
                </Colxx>
              </Row>
        </Colxx>
      </Row>
    </>
  );
};
export default injectIntl(ReportePages);