import React, { Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

const BlancoList = React.lazy(() =>
  import(/* webpackChunkName: "blanco-blanco-list" */ './blanco-list')
);

const BlancoListTwo = React.lazy(() =>
  import(/* webpackChunkName: "blanco-blanco-list-two" */ './blanco-list-two')
);
const ImageList = React.lazy(() =>
  import(/* webpackChunkName: "blanco-image-list" */ './image-list')
);
const ThumbList = React.lazy(() =>
  import(/* webpackChunkName: "blanco-thumb-list" */ './thumb-list')
);
const Details = React.lazy(() =>
  import(/* webpackChunkName: "blanco-details" */ './details')
);
const DetailsAlt = React.lazy(() =>
  import(/* webpackChunkName: "blanco-details-alt" */ './details-alt')
);

const BlancVer = React.lazy(() =>
  import(/* webpackChunkName: "blanco-blanco-ver" */ './blanco-ver')
);

const BlancoReporte = React.lazy(() =>
  import(/* webpackChunkName: "blanco-blanco-reporte" */ './blanco-reporte')
);

const Parametros = React.lazy(() =>
  import(/* webpackChunkName: "parametros" */ './parametros')
);

const PagesProduct = ({ match }) => (
  <Suspense fallback={<div className="loading" />}>
    <Switch>
      <Redirect exact from={`${match.url}/`} to={`${match.url}/blanco-list`} />
      <Route
        path={`${match.url}/blanco-list`}
        render={(props) => <BlancoList {...props} />}
      />
      <Route
        path={`${match.url}/blanco-list-two`}
        render={(props) => <BlancoListTwo {...props} />}
      />
      <Route
        path={`${match.url}/blanco-ver`}
        render={(props) => <BlancVer {...props} />}
      />
      <Route
        path={`${match.url}/blanco-reporte`}
        render={(props) => <BlancoReporte {...props} />}
      />
      <Route
        path={`${match.url}/parametros`}
        render={(props) => <Parametros {...props} />}
      />
      <Route
        path={`${match.url}/image-list`}
        render={(props) => <ImageList {...props} />}
      />
      <Route
        path={`${match.url}/thumb-list`}
        render={(props) => <ThumbList {...props} />}
      />
      <Route
        path={`${match.url}/details`}
        render={(props) => <Details {...props} />}
      />
      <Route
        path={`${match.url}/details-alt`}
        render={(props) => <DetailsAlt {...props} />}
      />
      <Redirect to="/error" />
    </Switch>
  </Suspense>
);
export default PagesProduct;
