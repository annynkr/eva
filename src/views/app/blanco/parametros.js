import React, {useState} from 'react';
import { Row } from 'reactstrap';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import Breadcrumb from '../../../containers/navs/Breadcrumb';
import ListParameter from '../../../containers/blanco/ListParameter';

const Parametros = ({ match }) => {
  const [parametroState, cambiarParametroState] = useState([
    {
      id: 1,
      title: 'Cheesecake',
    },
    {
      id: 2,
      title: 'Chocolate Cake',
    },
    {
      id: 3,
      title: 'Cremeschnitte',
    },
  ]);
  
  return (
    <>
      <Row>
        <Colxx xxs="12">
          <Breadcrumb heading="menu.parametros" match={match} />
          <Separator className="mb-5" />
        </Colxx>
      </Row>
      <Row>
        <Colxx xxs="12" md="6" className="mb-4">
          <ListParameter parametroState= {parametroState}/>
        </Colxx>

        <Colxx xxs="12" md="6" className="mb-4">
          <ListParameter parametroState= {parametroState}/>
        </Colxx>

        <Colxx xxs="12" md="6" className="mb-4">
          <ListParameter parametroState= {parametroState}/>
        </Colxx>

        <Colxx xxs="12" md="6" className="mb-4">
          <ListParameter parametroState= {parametroState}/>
        </Colxx>
      </Row>
    </>
  );
};

export default Parametros;
