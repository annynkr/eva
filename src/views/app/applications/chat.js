/* eslint-disable no-underscore-dangle */
/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect, useRef } from 'react';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Row } from 'reactstrap';

import { Colxx } from '../../../components/common/CustomBootstrap';

import {
  getContacts,
  getConversations,
  changeConversation,
  addMessageToConversation,
} from '../../../redux/actions';
import ChatApplicationMenu from '../../../containers/applications/ChatApplicationMenu';
import ChatHeading from '../../../components/applications/ChatHeading';
import MessageCard from '../../../components/applications/MessageCard';
import SaySomething from '../../../components/applications/SaySomething';
import {useDispatch,useSelector} from 'react-redux';
import {Chat,ChatInsert} from '../../../Dusk/DashboardDusk';
import Config from '../../../config/ConfigApi.json';


const ChatApp = ({
  intl,
  allContacts,
  conversations,
  loadingConversations,
  loadingContacts,
  currentUser,
  selectedUser,
  selectedUserId,

  getContactsAction,
  getConversationsAction,
  changeConversationAction,
  addMessageToConversationAction,
}) => {

  const disparador=useDispatch();
  const Dash1=useSelector(store=>store.Dash);


  const [activeTab, setActiveTab] = useState('messages');
  const [messageInput, setMessageInput] = useState('');
  const scrollBarRef = useRef(null);
  useEffect(() => {
    document.body.classList.add('no-footer');
    const currentUserId = 0;
    getContactsAction();
    getConversationsAction(currentUserId);

    return () => {
      document.body.classList.remove('no-footer');
    };
  }, [getContactsAction, getConversationsAction]);

  const focusScrollBottom = () => {
    setTimeout(() => {
      
        scrollBarRef.current._ps.element.scrollTop =
          scrollBarRef.current._ps.contentHeight;
      
    }, 1000);
  };

  useEffect(() => {
    if (loadingConversations && loadingContacts && selectedUser == null) {
      changeConversationAction(selectedUserId);
      focusScrollBottom();
    }
  }, [
    changeConversationAction,
    loadingContacts,
    loadingConversations,
    selectedUser,
    selectedUserId,
  ]);

  
  useEffect(() => {
    focusScrollBottom();
  }, []);
  useEffect(() => {
    focusScrollBottom();
  }, [Dash1.ChatObjetivo[0]]);

  const handleChatInputPress = (e) => {
    if (e.key === 'Enter') {
      if (messageInput.length > 0) {
        disparador(ChatInsert(Dash1.user.user[0].id_user,Dash1.ChatObjetivo[0].id_user,messageInput));
        setMessageInput('');
        setActiveTab('messages');
        focusScrollBottom();
        focusScrollBottom();
      }
    }
  };

  const handleSendButtonClick = () => {
    if (messageInput.length > 0) {
      addMessageToConversationAction(
        currentUser.id,
        selectedUser.id,
        messageInput,
        conversations
      );
      setMessageInput('');
      setActiveTab('messages');
      focusScrollBottom();
    }
  };

  const { messages } = intl;

  const selectedConversation =
    loadingConversations && loadingContacts && selectedUser
      ? conversations.find(
          (x) =>
            x.users.includes(currentUser.id) &&
            x.users.includes(selectedUser.id)
        )
      : null;

      useEffect(()=>{
        setTimeout(()=>{
          disparador(Chat(Dash1.user.user[0].id_user,Dash1.ChatObjetivo[0].id_user));
        }, 1000);
      });

  return (
    <>
      <Row className="app-row">
        <Colxx xxs="12" className="chat-app">
          <ChatHeading
            name={Dash1.ChatObjetivo[0].nombre}
            thumb={Config.baseurl+"user/"+Dash1.ChatObjetivo[0].foto}
            lastSeenDate={null}
          />
          

          {Dash1.Chat && (
            <PerfectScrollbar
              ref={scrollBarRef}
              // containerRef={(ref) => {}}
              // options={{ suppressScrollX: true, wheelPropagation: false }}
            >
              {Dash1.Chat.map((item, index) => {
                // const sender = allContacts.find((x) => x.id === item.sender);
                return (
                  <MessageCard
                    key={index}
                    sender={Dash1.ChatObjetivo[0]}
                    item={item}
                    currentUserid={Dash1.user.user[0]}
                  />
                );
              })}
            </PerfectScrollbar>
          )}
        </Colxx>
      </Row>
      {Dash1.Chat && (
        <>
        <SaySomething
          placeholder={messages['chat.saysomething']}
          messageInput={messageInput}
          handleChatInputPress={handleChatInputPress}
          handleChatInputChange={(e) => {
            setMessageInput(e.target.value);
          }}
          handleSendButtonClick={()=> {disparador(ChatInsert(Dash1.user.user[0].id_user,Dash1.ChatObjetivo[0].id_user,messageInput))
            setMessageInput('');
            setActiveTab('messages');
            focusScrollBottom();
          }}
        />
        <ChatApplicationMenu activeTab={activeTab} toggleAppMenu={setActiveTab} />
      </>
      )}
    </>
  );
};

const mapStateToProps = ({ chatApp }) => {
  const {
    allContacts,
    conversations,
    loadingConversations,
    loadingContacts,
    currentUser,
    selectedUser,
    selectedUserId,
  } = chatApp;

  return {
    allContacts,
    conversations,
    loadingConversations,
    loadingContacts,
    currentUser,
    selectedUser,
    selectedUserId,
  };
};
export default injectIntl(
  connect(mapStateToProps, {
    getContactsAction: getContacts,
    getConversationsAction: getConversations,
    changeConversationAction: changeConversation,
    addMessageToConversationAction: addMessageToConversation,
  })(ChatApp)
);
