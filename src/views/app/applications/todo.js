import React, { useState, useEffect } from 'react';
import {
  Row,
  Button,
  Collapse,
} from 'reactstrap';
import { NavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import IntlMessages from '../../../helpers/IntlMessages';
import { Colxx, Separator } from '../../../components/common/CustomBootstrap';
import {
  getTodoList,
  getTodoListWithOrder,
  getTodoListSearch,
  selectedTodoItemsChange,
} from '../../../redux/actions';
import TodoListItem from '../../../components/applications/TodoListItem';
import AddNewTodoModal from '../../../containers/applications/AddNewTodoModal';
import ApplicationMenu from '../../../components/common/ApplicationMenu';
import {useDispatch,useSelector} from 'react-redux';
import {UpdatePendiente} from '../../../Dusk/DashboardDusk';
import {DashboardCentral} from '../../../Dusk/DashboardDusk';


const TodoApp = ({
  match,
  intl,
  todoItems,
  searchKeyword,
  loading,
  orderColumn,
  orderColumns,
  selectedItems,
  getTodoListAction,
  getTodoListWithOrderAction,
  getTodoListSearchAction,
  selectedTodoItemsChangeAction,
}) => {
  
  const disparador=useDispatch();
  const [Dash,setDash]= useState(useSelector(store=>store.Dash).Central.Pendiente);
  const [Dash2,setDash1]= useState(useSelector(store=>store.Dash).Central.Pendiente);
  const [Busqueda,setBusqueda]= useState("");
  const [orden,setorden]= useState("All");
  const [usuario,setusuario]= useState(useSelector(store=>store.Dash).user);

  useEffect(() => {
    if(orden=="No"){
      Busqueda==""?setDash(Dash2.filter(item=>item.estado==0)):setDash(Dash2.filter(item=>(item.titulo.indexOf(Busqueda)>=0)&&(item.estado==0)))
    }else if(orden=="Si"){
      Busqueda==""?setDash(Dash2.filter(item=>item.estado==1)):setDash(Dash2.filter(item=>(item.titulo.indexOf(Busqueda)>=0)&&(item.estado==1)))
    }else if(orden=="All"){
      Busqueda==""?setDash(Dash2):setDash(Dash2.filter(item=>item.titulo.indexOf(Busqueda)>=0))
    }

  },[Busqueda,orden])

  const buscarAsyncDash=(e)=>{
    setBusqueda(e);
  }
  const ordenAsyncDash=(e)=>{
    setorden(e);
  }

  const [modalOpen, setModalOpen] = useState(false);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);
  const [lastChecked, setLastChecked] = useState(null);
  const [ItemSelect,setItemSelect]= useState({estado:"",id:""});


  useEffect(() => {
    document.body.classList.add('right-menu');
    getTodoListAction();

    return () => {
      document.body.classList.remove('right-menu');
    };
  }, [getTodoListAction]);

  useEffect(() => {
    if(lastChecked===true){
      setLastChecked(false);
      disparador(UpdatePendiente(ItemSelect.id,ItemSelect.estado,usuario.user[0].id_user));
    }
  },[lastChecked])

  const handleChangeSelectAll = () => {
    if (loading) {
      if (selectedItems.length >= Dash.length) {
        selectedTodoItemsChangeAction([]);
      } else {
        selectedTodoItemsChangeAction(Dash.map((x) => x.id_pendiente));
      }
    }
  };

  const { messages } = intl;

  return (
    <>
      <Row className="app-row survey-app">
        <Colxx xxs="12">
          <div className="mb-2">
            <h1>
              Pendientes
            </h1>
          </div>
          <div className="mb-2">
            <Button
              color="empty"
              className="pt-0 pl-0 d-inline-block d-md-none"
              onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
            >
              <IntlMessages id="todo.display-options" />{' '}
              <i className="simple-icon-arrow-down align-middle" />
            </Button>
            <Collapse
              id="displayOptions"
              className="d-md-block"
              isOpen={displayOptionsIsOpen}
            >
              <div className="d-block mb-2 d-md-inline-block">
                <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                  <input
                    type="text"
                    name=""
                    id="search"
                    placeholder={messages['menu.search']}
                    
                    onChange={(e) => {
                      setDash(Dash2) ;
                      buscarAsyncDash(e.target.value);
                    }}
                  />
                </div>
              </div>
            </Collapse>
          </div>
          <Separator className="mb-5" />
          <Row>
              {!Dash? "Cargando...":
                Dash.map((item, index) => (
                  <TodoListItem
                    key={`todo_item_${index}`}
                    item={item}
                    handleCheckChange={()=>{setItemSelect({id:item.id_pendiente,estado:item.estado==="0"?"1":"0"}); setLastChecked(true); }}
                    isSelected={item.estado=='0'?false:true}
                  />
                ))
              }
            
          </Row>
        </Colxx>
      </Row>
      <ApplicationMenu>
      <PerfectScrollbar>
        <div className="p-4">
          <p className="text-muted text-small">
            <IntlMessages id="todo.status" />
          </p>
          <ul className="list-unstyled mb-5">
            <NavItem>
              <NavLink
                location={{}}
                to="#"
                onClick={() => { ordenAsyncDash("All")}}
              >
                <i className="simple-icon-refresh" />
                  Todas las Pendientes
                <span className="float-right">
                </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to="#"
                onClick={() => { ordenAsyncDash("No")}}
              >
                <i className="simple-icon-refresh" />
                  Pendientes sin completar
                <span className="float-right">
                </span>
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                location={{}}
                to="#"
                onClick={() => { ordenAsyncDash("Si")}}
              >
                <i className="simple-icon-refresh" />
                  Pendientes Terminados
                <span className="float-right">
                </span>
              </NavLink>
            </NavItem>
          </ul>
         </div>
      </PerfectScrollbar>
    </ApplicationMenu>
      <AddNewTodoModal
        toggleModal={() => setModalOpen(!modalOpen)}
        modalOpen={modalOpen}
      />
    </>
  );
};

const mapStateToProps = ({ todoApp }) => {
  const {
   todoItems,
    searchKeyword,
    loading,
    orderColumn,
    orderColumns,
    selectedItems,
  } = todoApp;
  return {
    todoItems,
    searchKeyword,
    loading,
    orderColumn,
    orderColumns,
    selectedItems,
  };
};
export default injectIntl(
  connect(mapStateToProps, {
    getTodoListAction: getTodoList,
    getTodoListWithOrderAction: getTodoListWithOrder,
    getTodoListSearchAction: getTodoListSearch,
    selectedTodoItemsChangeAction: selectedTodoItemsChange,
  })(TodoApp)
);
