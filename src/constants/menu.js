import { adminRoot } from "./defaultValues";
// import { UserRole } from "../helpers/authHelper"

const data = [
  {
    id: 'dashboards',
    icon: 'iconsminds-shop-4',
    label: 'menu.dashboards',
    to: `${adminRoot}/dashboards`,
  },
  {
    id: 'pages-blanco',
    icon: 'iconsminds-paper',
    label: 'menu.blanco',
    to: `${adminRoot}/blanco`,
    subs: [
      {
        icon: 'simple-icon-list',
        label: 'menu.blanco-list',
        to: `${adminRoot}/blanco/blanco-list`,
      },
      {
        icon: 'simple-icon-graph',
        label: 'menu.blanco-reporte',
        to: `${adminRoot}/blanco/blanco-reporte`,
      },
      {
        icon: 'simple-icon-directions',
        label: 'menu.parametros',
        to: `${adminRoot}/blanco/parametros`,
      }
    ]
  },
  {
    id: 'blankpage',
    icon: 'iconsminds-bucket',
    label: 'menu.blank-page',
    to: `${adminRoot}/blank-page`,
  },
  {
    id: 'info',
    img: '/assets/logos/mobile.svg',
    label: 'menu.info',
    to: `${adminRoot}/info`,
  },
  // {
  //   id: 'docs',
  //   icon: 'iconsminds-library',
  //   label: 'menu.docs',
  //   to: 'https://gogo-react-docs.coloredstrategies.com/',
  //   newWindow: true,
  // },
];
export default data;
