const postsD = [
  {
    type: 'video',
    name: 'Mayra Sibley',
    date: '10 minutes ago',
    profilePic: '/assets/img/profiles/l-1.jpg',
    detail:
      'Keeping your eye on the ball while performing a deep dive on the start-up mentality.',
    image: '/assets/img/video/poster.jpg',
    video:
      'http://img-ys011.didistatic.com/static/didiglobal/do1_pcUZZjSG7vFlMbdr8fA6#.mp4',
    comments: [
      {
        name: 'Philip Nelms',
        detail:
          'Quisque consectetur lectus eros, sed sodales libero ornare cursus. Etiam elementum ut dolor eget hendrerit. Suspendisse eu lacus eu eros lacinia feugiat sit amet non purus.',
        date: 'Two hours ago',
        thumb: '/assets/img/profiles/l-4.jpg',
        likes: 5,
        key: 2,
      },
    ],
    key: 1,
  },
  {
    type: 'image',
    name: 'Mayra Sibley',
    date: '2 hours ago',
    profilePic: '/assets/img/profiles/l-1.jpg',
    detail:
      'Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality.',
    image: '/assets/img/details/5.jpg',
    video: '',
    comments: [
      {
        name: 'Latarsha Gama',
        detail:
          'Taking seamless key performance indicators offline to maximise the long tail.',
        date: 'Five days ago',
        thumb: '/assets/img/profiles/l-7.jpg',
        likes: 0,
        key: 4,
      },
    ],
    key: 2,
  },
  {
    type: 'text',
    name: 'Mayra Sibley',
    date: '3 hours ago',
    profilePic: '/assets/img/profiles/l-1.jpg',
    detail:
      'Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality.',
    image: '',
    video: '',
    comments: [],
    key: 3,
  },
  {
    type: 'image',
    name: 'Mayra Sibley',
    date: 'A day ago',
    profilePic: '/assets/img/profiles/l-1.jpg',
    detail:
      'Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality.',
    image: '/assets/img/details/1.jpg',
    video: '',
    comments: [
      {
        name: 'Latarsha Gama',
        detail:
          'Taking seamless key performance indicators offline to maximise the long tail.',
        date: 'Five days ago',
        thumb: '/assets/img/profiles/l-7.jpg',
        likes: 0,
        key: 4,
      },
      {
        name: 'Laree Munsch',
        detail:
          'Quisque consectetur lectus eros, sed sodales libero ornare cursus. Etiam elementum ut dolor eget hendrerit. Suspendisse eu lacus eu eros lacinia feugiat sit amet non purus.',
        date: 'Six days ago',
        thumb: '/assets/img/profiles/l-2.jpg',
        likes: 14,
        key: 5,
      },
    ],
    key: 4,
  },
];

export default postsD;
