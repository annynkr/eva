export const dataInteres = [
  {
    title: 'Seguridad',
    link: '#',
    icon: 'iconsminds-security-settings',
  },
  {
    title: 'Privacidad',
    link: '#',
    icon: 'iconsminds-male',
  },
  {
    title: 'Condiciones',
    link: '#',
    icon: 'iconsminds-newspaper',
  },
];
