export const users = [
  {
    user: 'Mayra Sibley',
    cargo: 'Director ejecutivo',
    thumb: '/assets/img/profiles/l-1.jpg',
    key: 0,
  },
  {
    user: 'Barbera Castiglia',
    cargo: 'Secretaria',
    thumb: '/assets/img/profiles/l-5.jpg',
    key: 1,
  },
  {
    user: 'Bao Hathaway',
    cargo: 'Jefe de operaciones',
    thumb: '/assets/img/profiles/l-3.jpg',
    key: 2,
  },
  {
    user: 'Lenna Majeed',
    cargo: 'Recepcionista',
    thumb: '/assets/img/profiles/l-4.jpg',
    key: 3,
  },
  {
    user: 'Esperanza Lodge',
    cargo: 'Vendedora',
    thumb: '/assets/img/profiles/l-5.jpg',
    key: 4,
  },
  {
    user: 'Brynn Bragg',
    cargo: 'Director técnico',
    thumb: '/assets/img/profiles/l-2.jpg',
    key: 5,
  },
  {
    user: 'Laree Munsch',
    cargo: 'Becaria',
    thumb: '/assets/img/profiles/l-1.jpg',
    key: 6,
  },
];
