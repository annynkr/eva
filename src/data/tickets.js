const tickets = [
  {
    id: '1',
    title: 'Mayra Sibley',
    detail: '09.08.2018 - 12:45',
    thumb: '/assets/img/profiles/l-1.jpg',
  },
  {
    id: '2',
    title: 'Mimi Carreira',
    detail: '05.08.2018 - 10:20',
    thumb: '/assets/img/profiles/l-2.jpg',
  },
  {
    id: '3',
    title: 'Philip Nelms',
    detail: '05.08.2018 - 09:12',
    thumb: '/assets/img/profiles/l-3.jpg',
  },
  {
    id: '4',
    title: 'Terese Threadgill',
    detail: '01.08.2018 - 18:20',
    thumb: '/assets/img/profiles/l-4.jpg',
  },
  {
    id: '5',
    title: 'Kathryn Mengel',
    detail: '27.07.2018 - 11:45',
    thumb: '/assets/img/profiles/l-5.jpg',
  },
  {
    id: '6',
    title: 'Esperanza Lodge',
    detail: '24.07.2018 - 15:00',
    thumb: '/assets/img/profiles/l-2.jpg',
  },
  {
    id: '7',
    title: 'Laree Munsch',
    detail: '24.05.2018 - 11:00',
    thumb: '/assets/img/profiles/l-1.jpg',
  },
];

export default tickets;
