export const ultimosConectados = [
  {
    title: 'Mayra Sibley',
    detail: '17.09.2018 - 04:45',
    thumb: '/assets/img/profiles/l-1.jpg',
    rate: 5,
    key: 0,
  },
  {
    title: 'Barbera Castiglia',
    detail: '15.08.2018 - 01:18',
    thumb: '/assets/img/profiles/l-2.jpg',
    rate: 4,
    key: 1,
  },
];
