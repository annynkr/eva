import React from 'react';
import {
  Modal,
  ModalBody,
  Button,
} from 'reactstrap';

const ConfimModal = ({ modalOpen, toggleModal }) => {
  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      size="sm"
    >
      <ModalBody>
        <p>
          <i 
            className="simple-icon-question" 
            style={{ color: 'red', marginRight: '5px' }}
          />
          ¿Estás seguro de eliminar?
        </p>


        <div className="float-right">
          <Button 
            outline
            color="dark"
            size="sm"
            onClick={toggleModal}
          >
            Cancelar
          </Button>{' '}

          <Button 
            color="primary"
            size="sm"
            onClick={toggleModal}
          >
            Sí, ¡adelante!
          </Button>{' '}
        </div>
      </ModalBody>
    </Modal>
  );
};

export default ConfimModal;
