import React from 'react';
import {
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import FormNewNota from '../../forms/FormNewNota';

const NewNoteModal = ({ modalOpen, toggleModal, cerrarModal }) => {
  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      size="lg"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="dashboards.newNota" />
      </ModalHeader>
      <ModalBody>
        <FormNewNota toggle={toggleModal}/>
      </ModalBody>
    </Modal>
  );
};

export default NewNoteModal;
