import React, { useState } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import FormNewNota from '../../forms/FormNewNota';
import FormNewNotaEdit from '../../forms/FormNewNoteEdit';

const ViewNewModal = ({ data,modalVOpen, toggleModal}) => {
  const setModalNestedContainer = useState(false);
  const [modalNested, setModalNested] = useState(false);
  const [closeAll, setCloseAll] = useState(false);
  return (
    <Modal
      isOpen={modalVOpen}
      toggle={toggleModal}
      size="lg"
      id="myModal"
    >
      <ModalHeader toggle={toggleModal}>
        <div className="m-2 float-left">
          <IntlMessages id="View" />
        </div>
        <div className="top-right-button-container m-2">
        </div>
      </ModalHeader>
      <ModalBody>
        <FormNewNotaEdit toggle={toggleModal} data={data} />
        <br />
        <Modal
            isOpen={modalNested}
            toggle={() => setModalNested(!modalNested)}
            onClosed={
              closeAll
                ? () => setModalNestedContainer(false)
                : () => {}
            }
          >
            <ModalHeader toggle={() => setModalNested(!modalNested)}><IntlMessages id="dashboards.newNota" /></ModalHeader>
            <ModalBody>
              <FormNewNota/>
            </ModalBody>
          </Modal>
      </ModalBody>
    </Modal>
  );
};

export default ViewNewModal;
