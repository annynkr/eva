import React, {useState} from 'react';
import {
    InputGroup,
    InputGroupAddon,
    Input,
    Button,
    Form,
} from 'reactstrap';
import ConfimModal from '../modals/ConfirmModal';

const Parametro = ({parametroItem}) => {
    const [modalOpen, setModalOpen] = useState(false);
    const [editandoParametro, cambiarEditandoParametro] = useState(false);
    const [nuevoParametro, cambiarNuevoParametro] = useState(parametroItem.title);

    const handleSubmit = (e) => {
        e.preventDefault();
        cambiarEditandoParametro(false);
    }

    return(
        <>
            <div className="d-flex flex-grow-1 min-width-zero mb-3 pb-3 border-bottom">
                <div className="list-item-heading mb-0 truncate w-80 w-xs-100  mb-1 mt-1 formulario-editar-parametro">
                    <span className="mt-2 mb-1 ml-3 text-muted"> <b>{parametroItem.id} </b></span>
                    { editandoParametro ?
                        <Form className="formulario-editar-parametro ml-2 mr-2" onSubmit={handleSubmit}>
                            <InputGroup className="comment-container">
                                <Input 
                                type="text"
                                value = {nuevoParametro}
                                onChange= {(e) => cambiarNuevoParametro(e.target.value)}
                                />
                                <InputGroupAddon addonType="append">
                                    <Button color="primary">
                                        <span className="d-inline-block">Actualizar</span>{' '}
                                    </Button>
                                </InputGroupAddon>
                            </InputGroup>
                        </Form>
                    : 
                        <span className="align-middle d-inline-block mt-2 ml-3"> {parametroItem.title}</span>
                    }
                </div>
            
                <div className="align-self-center mr-3 iconAccionParametro">
                <i 
                    className="simple-icon-pencil mr-3" 
                    onClick = {() => cambiarEditandoParametro(!editandoParametro)}
                />
                <i 
                    className="simple-icon-trash" 
                    onClick={() => setModalOpen(!modalOpen)}
                />
                </div>
            </div>
            <ConfimModal
                modalOpen={modalOpen}
                toggleModal={() => setModalOpen(!modalOpen)}
            />
        </>
    );
};

export default Parametro;