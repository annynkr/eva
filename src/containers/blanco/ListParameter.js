import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  InputGroup,
  InputGroupAddon,
  Input,
  Button,
  Form,
} from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import IntlMessages from '../../helpers/IntlMessages';
import Parametro from './Parametro';

const ListParameter = ({parametroState}) => {

  const handleSubmit = (e) => {
    e.preventDefault();
  }

  return (
    <>
      <Card>
        <CardBody>
          <CardTitle>
            <IntlMessages id="parametros.parametro" />
          </CardTitle>
          <div className="list-parametro">
            <PerfectScrollbar
              options={{ 
                suppressScrollX: true,
                suppressScrollY: false, 
                wheelPropagation : false,
                handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
              }}
            >
              {parametroState.length > 0 
                ? parametroState.map((parametroItem) => {
                  return ( 
                    <Parametro 
                      key={parametroItem.id}
                      parametroItem = {parametroItem}
                    />
                  );
                })
                : <p className="text-muted text-center">No hay parametros agregados</p>
              }
            </PerfectScrollbar>
          </div>
          <Form onSubmit={handleSubmit}>
            <InputGroup className="comment-container mt_3">
              <Input placeholder="Nuevo"/>
              <InputGroupAddon addonType="append">
                  <Button color="primary">
                      <span className="d-inline-block"><IntlMessages id="pages.create" /></span>{' '}
                  </Button>
              </InputGroupAddon>
            </InputGroup>
          </Form>
        </CardBody>
      </Card>
    </>
  )
}
export default ListParameter;


  