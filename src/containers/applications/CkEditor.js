import React, { useState } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';


const quillModules = {
  toolbar: [
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [
      { list: 'ordered' },
      { list: 'bullet' },
      { indent: '-1' },
      { indent: '+1' },
    ],
    ['link', 'image'],
    ['clean'],
  ],
};

const quillFormats = [
  'header',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
];

const CkEditor = (props) => {
  const [textQuillStandart, setTextQuillStandart] = useState('');

  return ( 
    props.value==null?
    <ReactQuill
        theme="snow"
        onChange={props.onChange}
        modules={quillModules}
        formats={quillFormats}
    />
    :
    <ReactQuill
        theme="snow"
        onChange={props.onChange}
        value={props.value}
        modules={quillModules}
        formats={quillFormats}
    />
  );
};

export default CkEditor;
