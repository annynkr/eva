/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Card, CardBody, CardTitle } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { NavLink } from 'react-router-dom';
import IntlMessages from '../../helpers/IntlMessages';
import config from '../../config/ConfigApi.json';
import {useDispatch,useSelector} from 'react-redux';
import {ChatObjetivo} from '../../Dusk/DashboardDusk';

const ContactToUser = (props) => {
  const disparador=useDispatch();
  const user=useSelector(store=>store.Dash)
  return (
    <Card className="contactToUser">
      <CardBody>
        <CardTitle>
          <IntlMessages id="dashboards.contactToUser" />
        </CardTitle>
        <div className="dashboard-list-contactToUser">
          <PerfectScrollbar
            options={{ 
              suppressScrollY: false, 
              wheelPropagation : false,
              handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
            }}
          >
            {props.info.map((item, index) => {
              if(user.user.user[0].id_user!=item.id_user){
                return (
                  <NavLink
                    to="chat"
                    onClick={()=>disparador(ChatObjetivo(item.id_user))}
                    key={`item${index}`}
                    className="list-item-heading mb-0 truncate w-80 w-xs-100  mb-1 mt-1"
                  >
                    <div
                      key={`item${index}`}
                      className="d-flex flex-row mb-3 pb-3 border-bottom"
                    >
                      <img
                        src={config.baseurl+"user/"+item.foto}
                        alt={"none"}
                        className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
                      /> 

                      <div className="pl-3 pr-2">
                        <p className="font-weight-medium mb-0">{item.nombre}</p>
                        <p className="text-muted mb-0 text-small">
                          {item.cargo}
                        </p>
                      </div>
                    </div>
                  </NavLink>
                );
              }
            })}
          </PerfectScrollbar>
        </div>
      </CardBody>
    </Card>
  );
};

export default ContactToUser;
