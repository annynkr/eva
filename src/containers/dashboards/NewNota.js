import React from 'react';
import {
  Card,
  CardBody,
  FormGroup,
  Button,
  Form,
  Input,
} from 'reactstrap';
import IntlMessages from '../../helpers/IntlMessages';


const NewNota = () => {

  return (
    <Card>
      <CardBody>
        {/* <CardTitle>
          <IntlMessages id="dashboards.newNota" />
        </CardTitle> */}
        <Form>
          <FormGroup>
            <label>
              <IntlMessages id="dashboards.newNota" />
            </label>
            <Input type="textarea" rows="2" />
          </FormGroup>

          <FormGroup>
            <Button color="primary" className="mt-2">
              <IntlMessages id="dashboards.create-note" />
            </Button>
          </FormGroup>
        </Form>
      </CardBody>
    </Card>
  );
};
export default NewNota;
