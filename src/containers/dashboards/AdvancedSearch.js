/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React, { useRef } from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
} from 'reactstrap';
import IntlMessages from '../../helpers/IntlMessages';
import DropzoneExample from '../../containers/forms/DropzoneExample';
import {useDispatch,useSelector} from 'react-redux';
import {Uploadimg} from '../../Dusk/DashboardDusk';

const AdvancedSearch = ({ messages }) => {
  const dropzone = useRef();
  const disparador=useDispatch();
  const usuario=useSelector(store=>store.Dash);

  const handleSubmit = (e) => {
    e.preventDefault();
    let drop=dropzone.current.myDropzone.files;
    let img="";
    let nam="";
    let menj=e.target.mensaje.value;
    if(drop.length==1){
      drop.map((file)=>{
        img=file.dataURL;
        nam=file.name; 
      })
    }else{ 
      img="nulo";
    }
    console.log(nam);
    // console.log(e.target.mensaje.value);
    disparador(Uploadimg(img,menj,usuario.user.user[0].id_user,nam));
    e.target.mensaje.value=""
    
  }


  return (
    <Card className="dashboard-search">
      <CardBody>
        <CardTitle className="text-white">
          <IntlMessages id="dashboards.advanced-search" />
        </CardTitle>
        <Form className="form-container" onSubmit={handleSubmit}>
          <FormGroup>
            <label>
              <IntlMessages id="dashboards.mensaje" />
            </label>
            <Input required name={"mensaje"} type="textarea" placeholder={messages['dashboards.mensaje']} />
          </FormGroup>
          <FormGroup>
            <Label>
              <IntlMessages id="dashboards.photo" />
            </Label>
            <div className="mb-3">
              <DropzoneExample ref={dropzone} />
            </div>
          </FormGroup>
          <FormGroup className="text-center">
            <Button color="primary" className="btn-lg">
              <IntlMessages id="dashboards.search" />
            </Button>
          </FormGroup>
        </Form>
      </CardBody>
    </Card>
  );
};
export default AdvancedSearch;
