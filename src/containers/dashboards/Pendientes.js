import React, { useState, useEffect } from 'react';
import {
    Card,
    CardBody,
    CardTitle,
    InputGroup,
    InputGroupAddon,
    Input,
    Button,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';
import PerfectScrollbar from 'react-perfect-scrollbar';
import IntlMessages from '../../helpers/IntlMessages';
import WidgetTodoListItem from '../../components/applications/WidgetTodoListItem';
import {useDispatch,useSelector} from 'react-redux';
import {InsertPendiente,UpdatePendiente} from '../../Dusk/DashboardDusk';

const Pendientes = (props) => {
  const disparador=useDispatch();
  const [lastChecked, setLastChecked] = useState(false);
  const [TituloNormal,setTituloNormal]= useState();
  const [ItemSelect,setItemSelect]= useState({estado:"",id:""});
  const usuario=useSelector(store=>store.Dash);
  const insertPendientre=async ()=>{
    disparador(InsertPendiente(usuario.user.user[0].id_user,TituloNormal));
    setTituloNormal("");
  }
 

  useEffect(() => {
    if(lastChecked===true){
      setLastChecked(false);
      disparador(UpdatePendiente(ItemSelect.id,ItemSelect.estado,usuario.user.user[0].id_user));
    }
  },[lastChecked])

  return (
    <>
      <Card className="h-510">
        <CardBody>
          <CardTitle>
              <NavLink to="todo">
                  <IntlMessages id="dashboards.pendiente" />
              </NavLink>
          </CardTitle>
          <div className="dashboard-list-with-pendientes">
            <PerfectScrollbar
              options={{ 
                  suppressScrollX: true,
                  suppressScrollY: false, 
                  wheelPropagation : false,
                  handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
              }}
            >
              {props.info.map((item, index) => (
                item.estado==="0"?
                <WidgetTodoListItem
                    key={`item${index}`}
                    item={item}
                    handleCheckChange={()=>{ setItemSelect({id:item.id_pendiente,estado:item.estado==="0"?"1":"0"}); setLastChecked(true); }}
                    isSelected={item.estado != "0"? true : false}
                />
                :null
              ))}
            
            </PerfectScrollbar>
          </div>

          <InputGroup className="comment-container mt_3">
            <Input placeholder="Nuevo pendiente" value={TituloNormal} onChange={(e)=>{ setTituloNormal(e.target.value) }}/>
            <InputGroupAddon addonType="append">
                <Button color="primary" onClick={(e)=>{ insertPendientre()}}>
                    <span className="d-inline-block"><IntlMessages id="pages.create" /></span>{' '}
                </Button>
            </InputGroupAddon>
          </InputGroup>
        </CardBody>
      </Card>
    </>
  );
};

export default Pendientes;

