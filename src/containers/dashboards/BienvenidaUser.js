import React,{useState,useEffect} from 'react';
import {
  Row,
  Card,
  CardBody,
  CardTitle,
  CardSubtitle,
  CardText,
} from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import { Colxx, Separator } from '../../components/common/CustomBootstrap';
import ThumbnailImage from '../../components/cards/ThumbnailImage';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Config from '../../config/ConfigApi.json';

const BienvenidaUser =  (props) => {
  const usuario=props.info;
  const [ip,setip]= useState(usuario.Central.log[0].ip);

function NAName() {
    if (navigator.userAgent.search("MSIE") >= 0) {
    NAName="IE";
    }
    else if (navigator.userAgent.search("Chrome") >= 0) {
      NAName="Chrome";
    }
    else if (navigator.userAgent.search("Firefox") >= 0) {
      NAName="Firefox";
    }
    else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
      NAName="Safari";
    }
    else if (navigator.userAgent.search("Opera") >= 0) {
      NAName="Opera";
    }
    return NAName;
  }

const OSName=()=>{  
  var OSName="Desconocido";
  var info=window.navigator.appVersion;
  if(info.indexOf("Windows") != -1){  OSName="Windows"};
  if(info.indexOf("Mac")!=-1) {OSName="MacOS"};
  if(info.indexOf("X11")!=-1){ OSName="UNIX"};
  if(info.indexOf("Linux")!=-1){ OSName="Linux"};

  return OSName
}
  return (
    <Row>
      <Colxx xxs="12">
        <Row>
          <Colxx lg="12" md="12" sm="12" xxs="12">
            <Card className="h-365">
              <CardTitle className="m-4">
                <IntlMessages id="dashboards.hello" />,
                <IntlMessages id="dashboards.welcome" />
              </CardTitle>
              <div className="d-flex flex-row mt_40">
                <ThumbnailImage
                  rounded
                  src={Config.baseurl+'user/'+usuario.user.user[0].foto}
                  alt="Card image cap"
                  className="m-4"
                />
                <CardBody className=" pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between">
                  <div className="min-width-zero">
                    <CardSubtitle className="truncate mb-1">
                      {usuario.user.user[0].nombre}
 
                    </CardSubtitle>
                    <CardText className="text-muted text-small mb-2">
                      {usuario.user.user[0].cargo}
                    </CardText>
                    <Separator className="mb-3" />

                    <CardText className="text-muted text-small mb-1">
                        <b>Hora:</b> {(new Date().toLocaleTimeString())}
                    </CardText>
                    
                    <CardText className="text-muted text-small mb-1">
                        <b>Fecha:</b> {(new Date().getDate() + "/" + (new Date().getMonth() +1) + "/" + new Date().getFullYear())}
                    </CardText>

                    <CardText className="text-muted text-small mb-1">
                      <b>Dirección IP: {ip} </b> 
                    </CardText>

                    <CardText className="text-muted text-small mb-1">
                      <b>Sisteme Operativo:</b> {OSName()}
                    </CardText>

                    <CardText className="text-muted text-small mb-1">
                      <b>Navegador:</b> {NAName()}
                    </CardText>
                  </div>
                </CardBody>
              </div>

              <CardBody className="mt_40 dashboard-list-user">
                  <PerfectScrollbar
                     options={{ 
                      suppressScrollX: true,
                      suppressScrollY: false, 
                      wheelPropagation : false,
                      handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
                    }}
                  >
                    {usuario.Central.log?usuario.Central.log.map((item, index) => {
                      return (
                        <div
                          key={`item${index}`}
                          className="d-flex flex-row mb-2 pt-2 border-top"
                        >
                          <img
                            src={Config.baseurl+'user/'+item.foto}
                            alt={item.nombre}
                            className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
                          />

                          <div className="pl-3 pr-2">
                            <p className="font-weight-medium mb-0">{item.nombre}</p>
                            <p className="text-muted mb-0 text-small">
                              {item.stamp}
                            </p>
                          </div>
                        </div>
                      );
                    }):null}
                  </PerfectScrollbar>
              </CardBody>
            </Card>
          </Colxx>
        </Row>
      </Colxx>
    </Row>
  );
};

export default BienvenidaUser;