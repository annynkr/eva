/* eslint-disable react/no-array-index-key */
import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';

import postsD from '../../data/postsDash';
import Post from '../../components/cards/Post';

const Social = (props) => {

  return (
    <>
        <div className="dashboard-list-with-social">
            <PerfectScrollbar
            options={{ 
                suppressScrollX: true,
                suppressScrollY: false, 
                wheelPropagation : false,
                handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
            }}
            >
                
                 {props.info.map((itemData,index) => {
                    return (
                        <Post
                            data={itemData}
                            key={`post_${itemData.id_post}`}
                            className="mb-4"
                        />
                    );
                })}
             </PerfectScrollbar>
        </div>
    </>
  );
};
export default Social;
