import React,{useEffect,useState} from 'react';
import GradientCard from '../../components/cards/GradientCard';

const GradientCardContainer = () => {
  const url='https://api.eva.desarrollodesoftwarerubiano.com/cartaaleatoria';
  const [Message,SetMessage]=useState();
  const fetchap= async()=> {
    const response = await fetch(url).then(response=>response.json()).then(Json=>{
      SetMessage(Json);
    });
  }
  useEffect(() => {
    fetchap();
  },[])

  return (
    <GradientCard>
      {
        !Message? "Cargando..."
       :
         Message.map((mensaje,index)=> {
          return <div key={mensaje.id_MensajeAleatorio}>
            <span  className="badge badge-pill badge-theme-3 align-self-start mb-3">
              {mensaje.etiqueta}
            </span>
            <p className="lead text-white">
              {mensaje.titulo}
            </p>
            <p className="text-white">
              {mensaje.subtitulo}
            </p>
          </div>
        })


      }
    </GradientCard>
  );
};
export default GradientCardContainer;
