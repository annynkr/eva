/* eslint-disable react/no-array-index-key */
import React from 'react';
import { Card, CardBody, CardTitle } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';

import IntlMessages from '../../helpers/IntlMessages';

import { comments } from '../../data/comments';

const Nota = ({ className = '', displayRate = false }) => {
  return (
    <Card className={className}>
      <CardBody>
        <CardTitle>
          <IntlMessages id="dashboards.nota" />
        </CardTitle>
        <div className="dashboard-list-with-user">
          <PerfectScrollbar
            options={{ 
              suppressScrollY: false, 
              wheelPropagation : false,
              handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
            }}
          >
            {comments.map((item, index) => {
              return (
                <div
                  key={`item${index}`}
                  className="d-flex flex-row mb-3 pb-3 border-bottom"
                >
                  <img
                    src={item.thumb}
                    alt={item.title}
                    className="img-thumbnail border-0 rounded-circle list-thumbnail align-self-center xsmall"
                  />

                  <div className="pl-3 pr-2">
                    <p className="text-muted mb-0 text-small">
                      {item.detail}
                    </p>
                    <p className="font-weight-medium mb-0">{item.title}</p>
                  </div>
                </div>
              );
            })}
          </PerfectScrollbar>
        </div>
      </CardBody>
    </Card>
  );
};

export default Nota;
