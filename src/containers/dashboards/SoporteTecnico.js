import React from 'react';
import {
  Card,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Button,
  Form,
  Input,
} from 'reactstrap';

import { Colxx } from '../../components/common/CustomBootstrap';
import IntlMessages from '../../helpers/IntlMessages';

const SoporteTecnico = () => {

  return (
    <Card>
      <CardBody>
        <CardTitle>
          <IntlMessages id="info.soporte" />
        </CardTitle>
        <Form className="dashboard-quick-post">
          <FormGroup row>
            <Label sm="3">
              <IntlMessages id="info.asunto" />
            </Label>
            <Colxx sm="9">
              <Input type="text" name="text" />
            </Colxx>
          </FormGroup>

          <FormGroup row>
            <Label sm="3">
              <IntlMessages id="info.mensaje" />
            </Label>
            <Colxx sm="9">
              <Input type="textarea" rows="3" />
            </Colxx>
          </FormGroup>

          <Button className="float-right" color="primary">
            <IntlMessages id="info.enviar" />
          </Button>
        </Form>
      </CardBody>
    </Card>
  );
};
export default SoporteTecnico;
