/* eslint-disable react/no-array-index-key */
import React, {useState} from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Card, CardBody, CardTitle } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import IntlMessages from '../../helpers/IntlMessages';
import data from '../../data/tickets';
import NewNoteModal from '../modals/dashboardNota/NewNoteModal';
import ViewNoteModal from '../modals/dashboardNota/ViewNoteModal';

const WidgetNota = (props) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalViewOpen, setModalViewOpen] = useState(false);
  const [notainfo,setnotainfo] = useState();

  return (
    <>
      <Card className="h-365">
        <div className="position-absolute card-top-buttons">
            <button
              type="button"
              className="btn btn-header-light icon-button"
              onClick={() => setModalOpen(!modalOpen)}
            >
            <i className="simple-icon-plus colorTheme"/>
          </button>
        </div>
        <CardBody>
          <CardTitle>
            <IntlMessages id="dashboards.nota" />
          </CardTitle>
          <div className="dashboard-list-with-user">
            <PerfectScrollbar
              options={{ 
                suppressScrollX: true,
                suppressScrollY: false, 
                wheelPropagation : false,
                handlers:['click-rail', 'drag-thumb', 'keyboard', 'wheel', 'touch']
            }}>
              {props.info.map((nota, index) => {
                return (
                  <NavLink
                    to="#"
                    key={`ticket${index}`}
                    className="list-item-heading mb-0 truncate w-80 w-xs-100  mb-1 mt-1"
                    onClick={() => {setnotainfo(nota); setModalViewOpen(!modalViewOpen)}}
                  >
                    <div
                      key={`ticket${index}`}
                      className="d-flex flex-row mb-3 pb-3 border-bottom"
                    >
                      <i 
                        className="iconsminds-file colorTheme" 
                        style={{
                          fontSize: "28px"
                        }}
                      />
                      <div className="pl-3 pr-2">
                        <p className="font-weight-medium mb-0 ">{nota.titulo}</p>
                        <p className="text-muted mb-0 text-small">
                          {nota.stamp}
                        </p>
                      </div>
                    </div>
                  </NavLink>
                );
              })}
            </PerfectScrollbar>
          </div>
        </CardBody>
      </Card>
      <NewNoteModal
        modalOpen={modalOpen}
        toggleModal={() => setModalOpen(!modalOpen)}
      />
      <ViewNoteModal
        modalVOpen={modalViewOpen}
        toggleModal={() => setModalViewOpen(!modalViewOpen)}
        data={notainfo}
      />
    </>
  );
};
export default WidgetNota;
