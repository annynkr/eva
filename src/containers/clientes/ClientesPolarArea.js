import React from 'react';
import { Card, CardBody, CardTitle, Button } from 'reactstrap';

import IntlMessages from '../../helpers/IntlMessages';
import { PolarAreaChart } from '../../components/charts';
import { NavLink } from 'react-router-dom';
import { polarAreaChartData } from '../../data/charts';

const ClientesPolarArea = ({ chartClass = 'chart-container' }) => {
  return (
    <Card>
      <div className="position-absolute card-top-buttons">
        <NavLink
          to="#"
        >
          <Button 
            outline
            color="primary"
            size="sm"
          >
            Acceder al modulo
          </Button>
        </NavLink>
      </div>
      <CardBody>
        <CardTitle>
          <IntlMessages id="clientes.moduloClientes" />
        </CardTitle>
        <div className={chartClass}>
          <PolarAreaChart shadow data={polarAreaChartData} />
        </div>
      </CardBody>
    </Card>
  );
};

export default ClientesPolarArea;