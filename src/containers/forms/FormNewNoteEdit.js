import React, {useState} from 'react';
import {
  Button,
  Input,
  FormGroup,
  Form,
  Modal, ModalHeader, ModalBody, ModalFooter 
} from 'reactstrap';
import IntlMessages from '../../helpers/IntlMessages';
import CkEditor from '../applications/CkEditor';
import ConfimModal from '../modals/ConfirmModal';
import {useDispatch,useSelector} from 'react-redux';
import {UpdateNota,DeleteNota} from '../../Dusk/DashboardDusk';


const FormNewNotaEdit = ({toggle,data}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [titulo,settitulo]= useState(data.titulo);
  const [descripcion,setdescripcion]= useState(data.descripcion);
  const Dash1=useSelector(store=>store.Dash);
  const disparador=useDispatch();

  const fun=() => {
      disparador(DeleteNota(Dash1.user.user[0].id_user,data.id_nota))
  }

  function handleSubmit(e) {
    // alert('Datos guardados exitosamente');
    e.preventDefault();
    disparador(UpdateNota(Dash1.user.user[0].id_user,titulo,descripcion,data.id_nota));
    toggle();
  }

  const [modal, setModal] = useState(false);

  const toggle1 = () => setModal(!modal);

  return ( 
    <>
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <label>
            <IntlMessages id="dashboards.newNotaTitle" />
          </label>
          <Input type="text" name="titulo" onChange={(e)=>{settitulo(e.target.value)}} value={titulo} />
        </FormGroup>

        <FormGroup>
          <label>
            <IntlMessages id="dashboards.newNotaNota" />
          </label>

          <CkEditor onChange={(e)=>{setdescripcion(e)}}  value={descripcion} />
        </FormGroup>
        
        <div className="float-right">
          <Button 
            outline
            color="danger"
            size="sm"
            onClick={() => {toggle1()}}
          >
            <IntlMessages id="blanco.delete" />
          </Button>{' '}

          <Button 
            color="primary"
            size="sm"
            type="submit"
          >
            <IntlMessages id="dashboards.edit-note" />
          </Button>{' '}
        </div>
      </Form>


      <Modal isOpen={modal} toggle={toggle1} className={"className"}>
        <ModalHeader toggle={toggle1}>Desea eliminar su nota? </ModalHeader>
        <ModalFooter>
          <Button color="primary" onClick={()=>{toggle(); fun(); toggle1()}}>Eliminar</Button>{' '}
          <Button color="secondary" onClick={toggle1}>Cancelar</Button>
        </ModalFooter>
      </Modal>
      

    </>
  );
};

export default FormNewNotaEdit;
