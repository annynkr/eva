import React, {useState,useReducer} from 'react';
import {
  Button,
  Input,
  FormGroup,
  Form,
} from 'reactstrap';
import IntlMessages from '../../helpers/IntlMessages';
import CkEditor from '../applications/CkEditor';
import Config from '../../config/ConfigApi.json';
import {useDispatch,useSelector} from 'react-redux';
import {DashboardCentral,InsertNota} from '../../Dusk/DashboardDusk';


const FormNewNota = (props) => {
  const disparador=useDispatch();
  const usuario=useSelector(store=>store.Dash);

  const [Nota,SetNota]= useState();
      

  function handleSubmit(e) {
    e.preventDefault();
    //$params['iduser'],$params['titulo'],$params['descripcion']
    disparador(InsertNota(usuario.user.user[0].id_user,Nota.titulo,Nota.mensaje));
    props.toggle();
  }

  return ( 
    <>
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <label>
            <IntlMessages id="dashboards.newNotaTitle" />
          </label>
          <Input type="text" onChange={(e)=>{ SetNota({...Nota,["titulo"]:e.target.value}) }}/>
        </FormGroup>

        <FormGroup>
          <label>
            <IntlMessages id="dashboards.newNotaNota" />
          </label>
          <CkEditor onChange={(e)=>{SetNota({...Nota,["mensaje"]:e.toString()})}}  />
        </FormGroup>

        <div className="float-right">
          <Button 
            color="primary"
          >
            <IntlMessages id="dashboards.create-note" />
          </Button>
        </div>
      </Form>
    </>
  );
};

export default FormNewNota;
