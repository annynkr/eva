/* eslint-disable react/no-array-index-key */
import React,{useEffect,useState} from 'react';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import notifications from '../../data/notifications';
import { adminRoot } from '../../constants/defaultValues';
import {useDispatch,useSelector} from 'react-redux';
import {DetectarNotificacion} from '../../Dusk/DashboardDusk';
import Config from '../../config/ConfigApi.json';

const NotificationItem = ({ usernombre,mensaje, stamp }) => {
  return (
    <div className="d-flex flex-row mb-3 pb-3 border-bottom">
      <NavLink to={`${adminRoot}/pages/product/details`}>
        <img
          src={Config.baseurl+"user/"+usernombre.foto}
          alt={mensaje}
          className="img-thumbnail list-thumbnail xsmall border-0 rounded-circle"
        />
      </NavLink>
      <div className="pl-3 pr-2">
        <NavLink to={`${adminRoot}/pages/product/details`}>
          <p className="font-weight-medium mb-1">Mensaje de {usernombre.nombre}</p>
        </NavLink>
      </div>
    </div>
  );
};

const TopnavNotifications = () => {
  const user=useSelector(store=>store.Dash);
  const disparador=useDispatch();
 
  useEffect(() => {
    if(user !== undefined) {
      setTimeout(() => {
        disparador(DetectarNotificacion(user.user.user[0].id_user));
      },6000)
    }
  })

const cuenta=() => {
  let oven=user.Notificacion.filter(item=>item.leido=="0");
  return oven.length;
}
  
  return (
    <div className="position-relative d-inline-block">
      <UncontrolledDropdown className="dropdown-menu-right">
        <DropdownToggle
          className="header-icon notificationButton"
          color="empty"
        >
          <i className="simple-icon-bell" />
          <span className="count"> {user.Notificacion?cuenta():null} </span>
        </DropdownToggle>
        <DropdownMenu
          className="position-absolute mt-3 scroll"
          right
          id="notificationDropdown"
        >
          <PerfectScrollbar
            options={{ suppressScrollX: true, wheelPropagation: false }}
          >
            {user.Notificacion?user.Notificacion.map((notification, index) => {
              if(notification.leido=="0"){
                let leco=user.Central.user.filter(item => item.id_user==notification.destino);
                return <NotificationItem key={index} usernombre={leco[0]}  {...notification} />;
              }

            }): null}
          </PerfectScrollbar>
        </DropdownMenu>
      </UncontrolledDropdown>
    </div>
  );
};

export default TopnavNotifications;
